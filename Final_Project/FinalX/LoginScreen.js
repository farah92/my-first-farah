import React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ImageBackground
} from 'react-native';

export default function LoginScreen({ navigation }) {

    return (

            <ImageBackground source={require('./assets/background.png')} style={styles.container}>
            <Text style={{ marginTop: 0, fontSize: 25, fontWeight: 'bold' }}>:: Login ::</Text>
           
            <View style={styles.inputText}>
                <MaterialIcons style={{ marginRight: 5 }} name="email" size={24} color="black" />
                <TextInput style={{ fontSize: 13 }}></TextInput>
            </View>

            <View style={styles.inputText}>
                <Entypo style={{ marginRight: 5 }} name="lock" size={24} color="black" />
                <TextInput  secureTextEntry={true} style={{ fontSize: 13 }}></TextInput>
            </View>

            <TouchableOpacity style={styles.buttonMasuk} onPress={() => navigation.push('Products')}>
                <Text style={{ backgroundColor: "#3EC6FF", color: "white", paddingHorizontal: 25, paddingVertical: 10, borderRadius: 100 }}>Masuk</Text>
            </TouchableOpacity>

            <View style={styles.footer}>
                <Image source={require('./logo.jpg')} style={styles.Logo}></Image>
            </View>
            </ImageBackground>
            
        // </View>

    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        // backgroundColor:'black'
    },
    text: {
        fontSize: 30, color: '#04063c'
    },
    inputText: {
        marginTop: 20,
        width: 200,
        height: 30,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: "#003366",
        flexDirection: "row"
    },
    buttonMasuk: {
        marginTop: 20,
        height: 30,
        alignItems: 'center',
    },
    buttonDaftar: {
        marginTop: 15,
        height: 30,
        alignItems: 'center',
    },
    footer: {
        marginTop: 50,
        height: 60,
        paddingTop:10,
        // backgroundColor: 'black',
        // elevation: 1,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        flex: 0,
        height: 100,
        resizeMode: "cover",
        justifyContent: "center"
    },
    text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"

    },
    Logo:{
        marginTop:230,
        width: 226,
        height: 100,
        resizeMode: 'contain',
      }
});
