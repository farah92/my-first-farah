console.log("===================== SOAL 1 ==================");
/*const golden = function goldenFunction(){
  console.log("this is golden!!")
}*/

const golden = () => {
	console.log("This is Golden");
}

golden();

console.log("===================== SOAL 2 ==================");

const nama = (a, b) => {
    return {
        fullname: () => { console.log(`${a} ${b}`) }

    }
}
nama("William", "Imoh").fullname()



console.log("===================== SOAL 3 ==================");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;
console.log(firstName, lastName, destination, occupation)



console.log("===================== SOAL 4 ==================");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined=[...west,...east]
console.log(combined)


console.log("===================== SOAL 5 ==================");

const planet = "earth"
const view = "glass"

const before = 'Lorem ' + `${view}`+ 'dolor sit amet, ' +  
'consectetur adipiscing elit,' + `${planet}` + 'do eiusmod tempor ' +
'incididunt ut labore et dolore magna aliqua. Ut enim' +
' ad minim veniam'
console.log(before)