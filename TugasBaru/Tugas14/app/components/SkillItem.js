import { StatusBar } from 'expo-status-bar';
import React , { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillItem extends React.Component {
    render() {
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <View style={styles.ListKategori}>
                        <Icon2 style={styles.LogoKategori} size={70} name={skill.iconName} />
                        <View style={{padding:15}}>
                            <Text style={{color:'#003366',fontSize:20,fontWeight: 'bold',flex:4}}>{skill.skillName}</Text>
                            <Text style={{color:'#3EC6FF',fontSize:15,flex:4}}>{skill.categoryName}</Text>
                            <View style={{alignItems:'flex-end'}}>
                                <Text style={{color:'white',fontSize:30,fontWeight: 'bold'}}>{skill.percentageProgress}</Text>
                            </View>
                        </View>
                        <Icon2 name="chevron-right" style={styles.LogoKategori} size={100}/>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo:{
    width: 200,
    height: 60,
    
  },
  kontak:{
    marginTop:30,
    marginLeft:10,
    flexDirection:'row'
  },
  circle: {
    // position: 'absolute',
    bottom: 10,
    top:1,
    left: 5,
    backgroundColor: '#B4E9FF',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#B4E9FF',
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  kontakName:{
    // backgroundColor: '#B4E9FF',
    marginLeft:15
  },
  SkillTitle:{
    margin:12,
    marginBottom:0
 },
  Kategori:{
      flexDirection:'row',
      padding:10
  },
  
  ButtonKategori:{
      backgroundColor:'#B4E9FF',
      padding:7,
      margin:1,
      borderRadius: 20,
  },
  ListKategori:{
    backgroundColor:'#B4E9FF',
    flexDirection:'row',
    margin:10,
    marginTop:0
  },
  LogoKategori:{
      padding:5,
      color:'#003366',
      flex:2
      
  }

  
});

