console.log('====== TUGAS 1 (Looping While)=======');
console.log('====== LOOPING PERTAMA =======');
var angka = 1;
while(angka <= 20)
{
	if(angka%2 === 0)
	{
		console.log(angka + '- I Love Coding');
	}
	angka++;
}

console.log('====== LOOPING KEDUA =======');
var angka = 20;
while(angka >= 1)
{
	if(angka%2 === 0)
	{
		console.log(angka + '- I will become a mobile developer');
	}
	angka--;
}

console.log('\n');

console.log('====== TUGAS 2 (Looping For) =======');

for(var angka = 1; angka <= 20; angka++){

	if(angka % 3 == 0)
	{
    	console.log(angka + '- I love Coding');
	}
	else if(angka%2 == 1)
	{
		console.log(angka + '- Santai');
	}
  	
  		angka++
  		console.log(angka + '- Berkualitas');
}




console.log('\n');
console.log('====== TUGAS 3 (Persegi Panjang #) =======');

var s = '';
for(var i = 0; i < 4; i++) //baris
{
	for(var j = 0; j < 8; j++) //kolom
	{
		s += '#';
	}
	s += '\n';
}


console.log(s);

console.log('\n');
console.log('====== TUGAS 4 (Membuat Tangga) =======');

var s = '';
for(var i = 0; i < 7; i++) //baris
{
	for(var j = 0; j <= i; j++) //kolom
	{
		s += '#';
	}
	s += '\n';
}


console.log(s);

console.log('\n');
console.log('====== TUGAS 5 (Membuat Papan Catur) =======');

var nilaiAwal = 0;
var nilaiAkhir = 0;
for(nilaiAwal; nilaiAwal < 8; nilaiAwal++)
{
	var nilaiTengah = 0;
	var papanCatur = '';
	for(nilaiTengah; nilaiTengah < 8; nilaiTengah++)
	{
		if(nilaiAkhir % 2 == 1)
		{
			papanCatur += '#';
		}
		else
		{
			papanCatur += ' ';
		}
		nilaiAkhir++;
	}
	console.log(papanCatur);
	nilaiAkhir++;
}

