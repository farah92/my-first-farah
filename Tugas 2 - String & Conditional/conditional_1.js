var nama = 'Farah';
var peran = 'Werewolf';
//======= CASE 1 =========
if(nama == '' && peran == '')
{
	console.log("Nama harus diisi!");
}

//======= CASE 2 =========
else if(nama != '' && peran == '')
{
	console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
}

//======= CASE 3 =========
else if(nama != '' && peran == 'Penyihir')
{
	console.log("Selamat datang di Dunia Werewolf, " + nama);
	console.log("Halo Penyihir "+nama+" , kamu dapat melihat siapa yang menjadi werewolf!");
}

//======= CASE 4 =========
else if(nama != '' && peran == 'Guard')
{
	console.log("Selamat datang di Dunia Werewolf, " + nama);
	console.log("Halo Guard "+nama+" , kamu akan membantu melindungi temanmu dari serangan werewolf.");
}

//======= CASE 5 =========
else if(nama != '' && peran == 'Werewolf')
{
	console.log("Selamat datang di Dunia Werewolf, " + nama);
	console.log("Halo Werewolf "+nama+" , Kamu akan memakan mangsa setiap malam!" );
}
else
{
	console.log("Isi Nama & Peran yaa ");
}

console.log('===== Switch Case ======');
var hari = 21; 
var bulan = 3; 
var tahun = 1945;

switch(bulan)
{
	case 1 : { bulan = 'Januari'; break; }
	case 2 : { bulan = 'Februari'; break; }
	case 3 : { bulan = 'Maret '; break; }
	case 4 : { bulan = 'April '; break; }
	case 5 : { bulan = 'Mei '; break; }
	case 6 : { bulan = 'Juni '; break; }
	case 7 : { bulan = 'Juli '; break; }
	case 8 : { bulan = 'Agustus '; break; }
	case 9 : { bulan = 'September '; break; }
	case 10 : { bulan ='Oktober '; break; }
	case 11 : { bulan ='November '; break; }
	case 12 : { bulan ='Desember '; break;}
	default : { bulan = 'Tolong Ganti Bulan !!'; }
}	

console.log(hari+' '+bulan+' '+tahun);


var hari = 11; 
var bulan = 6; 
var tahun = 1990;

if(hari >= 1 && hari <= 31) { NilaiHari = 1; }
else { NilaiHari = 0; }

switch(NilaiHari)
{
	case NilaiHari = 1    : { hari = hari; break; }
	default 		 	  : { hari =  '(Isikan range 1 - 31)';}
}

switch(true)
{
	case bulan == 1 : { bulan = 'Januari'; break; }
	case bulan == 2 : { bulan = 'Februari'; break; }
	case bulan == 3 : { bulan = 'Maret '; break; }
	case bulan == 4 : { bulan = 'April '; break; }
	case bulan == 5 : { bulan = 'Mei '; break; }
	case bulan == 6 : { bulan = 'Juni '; break; }
	case bulan == 7 : { bulan = 'Juli '; break; }
	case bulan == 8 : { bulan = 'Agustus '; break; }
	case bulan == 9 : { bulan = 'September '; break; }
	case bulan == 10 : { bulan ='Oktober '; break; }
	case bulan == 11 : { bulan ='November '; break; }
	case bulan == 12 : { bulan ='Desember '; break;}
	default 		 : { bulan = '(Isikan range 1 - 12)'; }
}

if(tahun >= 1990 && tahun <= 2200) { NilaiTahun = 1; }
else { NilaiTahun = 0; }

switch(NilaiTahun)
{
	case NilaiTahun = 1    : { tahun = tahun; break; }
	default 		 : { tahun =  '(Isikan range 1990 - 2200)';}
}

console.log(hari +' '+ bulan+ ' ' + tahun);
