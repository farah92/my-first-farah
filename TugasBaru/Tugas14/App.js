import { StatusBar } from 'expo-status-bar';
import React , {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Main from './app/components/Main'
import SkillScreen from './app/components/SkillScreen'

export default class App extends Component{
  render() {
    return(
      <Main/>
      // <SkillScreen/>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
