console.log("==================== Soal No. 1 ============================");
function balikString(kata)
{
	var akhir = '';
	var panjang = kata.length;
	for(var i = panjang-1; i >= 0; i--)
	{
		akhir += kata.charAt(i);
	}
	return akhir
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


console.log("==================== Soal No. 2 ============================");
function palindrome(kata) {
  var akhir = '';
	var panjang = kata.length;
	for(var i = panjang-1; i >= 0; i--)
	{
		akhir += kata.charAt(i);
	}
	if(akhir == kata)
	{
		return true;
	}
	else
	{
		return false;
	}
}


console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false


console.log("==================== Soal No. 3 ============================");
function bandingkan(n1, n2) {

  if(n1 <= 0 || n2 <= 0)
  {
  	return -1;
  }
  else if(n2 == undefined)
  {
  	return -1;
  }
  else if(n1 == undefined ||  n2 == undefined)
  {
  	return 1;
  }
  
  else if(n1 == n2)
  {
  	return -1;
  }
  else if(n1 > n2)
  {
  	return n1;
  }
  
  else
  {
  	return n2;
  }
  
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18