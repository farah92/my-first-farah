
import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator,
ImageBackground , TouchableOpacity } from 'react-native';
import Axios from 'axios';
import { Entypo } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

export default class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id:this.props.route.params.id - 1, //ini di ganti ga ?
      data: {},
      isLoading: true,
      isError: false
    };
  }



  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }
1
  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`https://myfarah-fbeb8.firebaseio.com/produk/${this.state.id}.json`)
      this.setState({ isError: false, isLoading: false, data: response.data })
      // alert(response.data.category);
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // alert(this.state.data) 
    
    // If data finish load
    return(
        
        // alert(this.props.route.params.id),
        //alert(_.filter(data, { id: 5})),
//         data = this.state.data[2].id,
// alert(data),

        <ImageBackground
        source={require('./assets/bg2.jpg')}
            style={styles.container}>
                
            <View style={styles.header}>
                <Text style={styles.headerText}>{this.state.data.productName}</Text>
            </View>
            <View style={styles.content}>
                <View style={styles.category}>
                <Image source={{ uri: this.state.data.gambar }} style={styles.Image} />
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} >Product Name</Text></View>
                        <View flex={1} ><Text style={styles.listText}>:</Text></View>
                        <View flex={4} ><Text style={styles.listText}>{this.state.data.productName}</Text></View>
                    </View>
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} >Product Code</Text></View>
                        <View flex={1} ><Text style={styles.listText}>:</Text></View>
                        <View flex={4} ><Text style={styles.listText}>{this.state.data.productID}</Text></View>
                    </View>
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} >Since</Text></View>
                        <View flex={1} ><Text style={styles.listText}>:</Text></View>
                        <View flex={4} ><Text style={styles.listText}>{this.state.data.since}</Text></View>
                    </View>
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} >Category</Text></View>
                        <View flex={1} ><Text style={styles.listText}>:</Text></View>
                        <View flex={4} ><Text style={styles.listText}>{this.state.data.category}</Text></View>
                    </View>
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} >Dimension</Text></View>
                        <View flex={1} ><Text style={styles.listText}>:</Text></View>
                        <View flex={4} ><Text style={styles.listText}>P. {this.state.data.panjang}</Text></View>
                    </View>
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} ></Text></View>
                        <View flex={1} ><Text style={styles.listText}></Text></View>
                        <View flex={4} ><Text style={styles.listText}>L. {this.state.data.lebar}</Text></View>
                    </View>
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} ></Text></View>
                        <View flex={1} ><Text style={styles.listText}></Text></View>
                        <View flex={4} ><Text style={styles.listText}>T. {this.state.data.tinggi}</Text></View>
                    </View>
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} >Min. Order</Text></View>
                        <View flex={1} ><Text style={styles.listText}>:</Text></View>
                        <View flex={4} ><Text style={styles.listText}>{this.state.data.min_order}</Text></View>
                    </View>
                    <View style={styles.list}>
                        <View flex={4} ><Text style={styles.listText} >Price</Text></View>
                        <View flex={1} ><Text style={styles.listText}>:</Text></View>
                        <View flex={4} ><Text style={styles.listText}>{this.state.data.price}</Text></View>
                    </View>
                    <View style={styles.Button}>
                        <View>
                            <TouchableOpacity  style={styles.appButtonContainerGreen}>
                                <Text style={styles.appButtonText}>Order</Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity  style={styles.appButtonContainer}>
                                <Text style={styles.appButtonText}>Back</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>


               

                
            </View>
        </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: undefined,
      height: undefined,
      backgroundColor:'transparent',
      paddingBottom:20
      // justifyContent: 'center',
      // alignItems: 'center',
    //   paddingTop: 10
    },
    header:{
        height:60,
        backgroundColor:'#FFFFFF',
        padding: 20,
        alignItems:'center'
    },
    headerText:{
        color:'#000080',
        fontSize:20,
        fontWeight:'bold'

    },
    content:{
        alignItems:'center',
        margin:15,
        flex:1,
        backgroundColor:'#FFFFFF'
    },
    category:{
        width:'100%',
        paddingTop:20,
        alignItems:'center'
    },
    Judul:{
        fontSize:24,
        fontWeight:'bold',
        color:'#000080',
        marginBottom:15
    },
    list:{
        flexDirection:'row',
        alignItems:'center',
        left:30,
        right:30,
        bottom:10

    },
    listKiri:{
        width: '45%',
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        height:170,
        marginRight:20
    },
    Button:{
        flexDirection:'row',
        alignItems: 'center',
    },
    Image:{
        // position: 'absolute',
        bottom: 100,
        // left: 50,
        top:20,
        backgroundColor: '#ff0080',
        // borderRadius: 100,
        // borderWidth: 2,
        // borderColor: '#ff0080',
        width: 150,
        height: 100,
        // alignItems: 'center',
        marginBottom:60
        // justifyContent: 'center',
    },
    listText:{
        // fontWeight:'bold',
        fontSize:18,
        marginBottom:10
        // left:50
    },
    appButtonContainer: {
        // elevation: 8,
        backgroundColor: "#000080",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 30,
        // marginTop:30,
        alignSelf:'center',
        marginRight:20

      },

      appButtonContainerGreen: {
        // elevation: 8,
        backgroundColor: "#2DB52A",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 30,
        // marginTop:30,
        alignSelf:'center',
        marginRight:20

      },
      appButtonText: {
        fontSize: 16,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
      },

})