import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-gesture-handler';
import AboutScreen from './AboutScreen'
import LoginScreen from './LoginScreen'
import Product from './SkillScreen'
import Detail from './Detail'

const Stack = createStackNavigator()
const ProducStack = createStackNavigator()
// const Tabs = createBottomTabNavigator()
// const Drawer = createDrawerNavigator()

const ProducStackScreen = () => {// ini di panggil
    return (
      <ProducStack.Navigator>
        <ProducStack.Screen name="Products" component={Product} options={{ headerShown: false }} />
         <ProducStack.Screen name="Details" component={Detail} options={{ title: 'Detail' }}
          options={({ route }) => ({
            id: route.params.id
          })}
        />
        {/* <ProducStack.Screen name="DetailsUser" component={DetailUser} options={{ title: 'Detail User' }}
          options={({ route }) => ({
            title: route.params.name
          })}
        /> */} 
      </ProducStack.Navigator>
    );
    }

// const TabsScreen = () => (
    // <Tabs.Navigator >
   
        {/* <Tabs.Screen name="SkillScreen" component={SkillScreen}  /> */}
        {/* <Tabs.Screen name="ProjectScreen" component={ProjectScreen} /> */}
        {/* <Tabs.Screen name="AddScreen" component={AddScreen} /> */}
    {/* </Tabs.Navigator> */}
// );


const Drawer = createDrawerNavigator(); // drawer ku yg ini
const Products = () => (
  <Drawer.Navigator initialRouteName="Profile">
    <Drawer.Screen name="Products" component={ProducStackScreen} />
    <Drawer.Screen name="About Us" component={AboutScreen} />
    <Drawer.Screen name="Logout" component={LoginScreen} />
  </Drawer.Navigator>

// const DrawerScreen = () => (
//     <Drawer.Navigator screenOptions={{header: () => null}}>

//         <Drawer.Screen name="Home" component={TabsScreen}  />
//         <Drawer.Screen name="About Us" component={AboutScreen} />
//     </Drawer.Navigator>
);



export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login Screen">
                <Stack.Screen name="Login" component={LoginScreen}  />
                <Stack.Screen name="Products" component={Products} />
                <Stack.Screen name="Logout" component={LoginScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}