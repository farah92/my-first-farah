import { StatusBar } from 'expo-status-bar';
import React , {Component} from 'react';
import { StyleSheet, Text, View, Image, TextInput ,  TouchableOpacity , FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

export default class AboutScreen extends Component{
  // alert(data.kind);
  
  render(){
  return (
    <View style={styles.container}>
      <Text style={{fontSize:25, color:"#003366"}}>Tentang Saya</Text>
      <View style={styles.circle}>
        <Icon name="person" size={50} style={{textAlign:'center',padding:30}}></Icon>
      </View>
      <Text style={{fontSize:20, color:"#003366"}}>Farah Afifah</Text>
      <Text style={{fontSize:15, color:"#3EC6FF"}}>React Native Developer</Text>
      <View style={styles.Detail}>
        <Text>Portofolio</Text>
        <View style={{borderBottomColor: '#003366',borderBottomWidth: 1,paddingTop:5}}/>
        <View style={styles.ImageDetailRow}>
          <View style={styles.ImageDetailText}>
            <Icon2 name="gitlab" size={40} style={{paddingLeft:15}}></Icon>
            <Text style={{fontSize:15, color:"#003366"}}>@farah92</Text>
          </View>
          <View style={styles.ImageDetailText}>
            <Icon2 name="github" size={40} style={{marginLeft:80}}></Icon>
            <Text style={{fontSize:15, color:"#003366", marginLeft:70}}>@farah1992</Text>
          </View>
        </View>
      </View>
      <View style={styles.Detail2}>
        <Text>Hubungi Saya</Text>
        <View style={{borderBottomColor: '#003366',borderBottomWidth: 1,paddingTop:5}}/>
        <View style={styles.ImageDetailCoulumn}>
          <View style={styles.ImageDetailTextColumn}>
            <Icon2 name="facebook" size={40} style={{marginLeft:60}}></Icon>
            <Text style={{fontSize:15, color:"#003366",paddingTop:10,paddingLeft:10}}>@farah92</Text>
          </View>
          <View style={styles.ImageDetailTextColumn}>
            <Icon2 name="instagram" size={40} style={{marginLeft:60}}></Icon>
            <Text style={{fontSize:15, color:"#003366",paddingTop:10,paddingLeft:10}}>@farah_afifah92</Text>
          </View>
          <View style={styles.ImageDetailTextColumn}>
            <Icon2 name="twitter" size={40} style={{marginLeft:60}}></Icon>
            <Text style={{fontSize:15, color:"#003366",paddingTop:10,paddingLeft:10}}>@farah_fafa_a</Text>
          </View>
        </View>
      </View>
    </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:50,
    // backgroundColor: '#fff',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  Detail:{
    backgroundColor:"#EFEFEF",
    padding:5,
    width:300,
    marginTop:30
  },
  ImageDetailRow:{
    flexDirection: 'row',
    justifyContent:"center",
    padding:10
  },
  ImageDetailText:{
    flexDirection:'column'
  },
  Detail2:{
    backgroundColor:"#EFEFEF",
    width:300,
    marginTop: 40,
    padding:5
  },
  ImageDetailColumn:{
    flexDirection: 'column',
    justifyContent:"center",
    padding:10
  },
  ImageDetailTextColumn:{
    flexDirection:'row'
  },
  circle: {
    bottom: 10,
    left: 5,
    backgroundColor: '#EFEFEF',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#EFEFEF',
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:30
  },

 
});
