import { StatusBar } from 'expo-status-bar';
import React , {Component} from 'react';
import { StyleSheet, Text, View  } from 'react-native';
import LoginScreen from './komponen/LoginScreen';
import RegisterScreen from './komponen/RegisterScreen';
import AboutScreen from './komponen/AboutScreen';

export default class App extends Component{
  // alert(data.kind);
  
  render(){
  return (
    <View style={styles.container}>
      {/* <LoginScreen/> */}
      <RegisterScreen/>
      {/* <AboutScreen /> */}
    </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
