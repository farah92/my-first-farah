import React from 'react';
import { View, Text, TextInput, StyleSheet, Button , ImageBackground , Image , TouchableOpacity } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
// import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default class AboutScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)
    if (this.state.password === '12345678') {
      this.props.navigation.push('Home', { userName: this.state.userName })
    } else {
      this.setState({ isError: true })
    }
  }

  render() {
    return (
      <ImageBackground
        source={require('./assets/background.png')}
        style={styles.container}>
            <View style={styles.header}>
                    <Text style={styles.headerText}>MY PROFILE</Text>
                </View>
        <View style={styles.content}>
            <View style={styles.content1}>
                <View ><Image source={require('./assets/farah.jpg')} style={styles.foto}></Image></View>
                
            </View>
            <View>
                    <Text style={styles.textDesc}>FARAH AFIFAH</Text>
                    <Text style={styles.textDescBlue}>SYSTEM DEVELOPMENT</Text>
                </View>
                        <View style={styles.list}>
                            <View flex={2} ><Text style={styles.listText} >Email</Text></View>
                            <View flex={1} ><Text style={styles.listText}>:</Text></View>
                            <View flex={4} ><Text style={styles.listText}>farah@padma.co.id</Text></View>
                        </View>
                        <View style={styles.list}>
                            <View flex={2} ><Text style={styles.listText} >Instagram</Text></View>
                            <View flex={1} ><Text style={styles.listText}>:</Text></View>
                            <View flex={4} ><Text style={styles.listText}>@farah_afifah92</Text></View>
                        </View>
                        <View style={styles.list}>
                            <View flex={2} ><Text style={styles.listText} >Gitlab</Text></View>
                            <View flex={1} ><Text style={styles.listText}>:</Text></View>
                            <View flex={4} ><Text style={styles.listText}>@farah92</Text></View>
                        </View>
      
        </View>
        <View style={styles.LogoStyle}>
            <Image source={require('./logo.jpg')} style={styles.LogoBottom}></Image>
        </View>
      </ImageBackground>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: undefined,
    height: undefined,
    backgroundColor:'transparent',
    // justifyContent: 'center',
    alignItems: 'center',
  },
  header:{
    height:40,
    width:'100%',
    backgroundColor:'#FFFFFF',
    padding: 10,
    alignItems:'center',
    marginBottom:10
    },
    headerText:{
        color:'#000080',
        fontSize:20,
        fontWeight:'bold'

    },
  content:{
    alignItems: 'center',
    
  },
  content1:{
    flexDirection:'row',
    // backgroundColor:'white',
    width:'100%',
    padding:40
  },
  foto:{
      // marginRight:40,
      width: 112,
    height: 138,
    // resizeMode: 'contain',
  },
  textDesc:{
    // marginTop:10,
    // marginBottom:20,
    fontWeight:'bold',
    color:'#FFFFFF',
    fontSize:18
  },
  textDescBlue:{
    marginBottom:20,
    fontWeight:'bold',
    color:'#000080',
    fontSize:18
  },
  welcome:{
    fontSize:36,
    color:'white',
    fontWeight:'bold',
    marginBottom:80,
    paddingTop: 130
  },
  textInput:{
    backgroundColor:'white',
    width:300,
    padding:5
  },
  judulText:{
    color:'white',
    paddingBottom:5,
    fontWeight:'bold',
    paddingTop:10,
    fontSize:18,
  },
  ButtonInput:{
    paddingTop:40,
    alignSelf:'flex-end',
    paddingRight:80
  },
  appButtonContainer: {
    // elevation: 8,
    backgroundColor: "#000080",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginTop:30,
    alignSelf:'flex-end',
    marginRight:80
  },
  appButtonText: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
  },
  LogoStyle:{
    alignItems:'center',
  },
  Logo:{
    width: 112,
    height: 138,
    resizeMode: 'contain',
    // backgroundColor:'red',
  },
  list:{
    flexDirection:'row',
    alignItems:'center',
    // left:30,
    // right:30,
    // bottom:10,
    margin:10,
    borderBottomColor: "#000080",
    borderBottomWidth: 3,
    // backgroundColor:'red'

},
listText:{
    // fontWeight:'bold',
    fontSize:18,
    marginBottom:10,
    color:'#000080'
    // left:50
},
LogoBottom:{
    marginTop:30,
    width: 296,
    height: 80,
    resizeMode: 'contain',
}
  
});
