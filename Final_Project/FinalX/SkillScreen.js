
import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator,
ImageBackground , TouchableOpacity } from 'react-native';
import Axios from 'axios';
import { Entypo } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'


import Detail from './Detail'
import { ScrollView } from 'react-native-gesture-handler';

export default class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }

  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`https://myfarah-fbeb8.firebaseio.com/produk.json`)
      this.setState({ isError: false, isLoading: false, data: response.data })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (
      <ScrollView>
        <ImageBackground source={require('./assets/bg2.jpg')} style={styles.container}>
            {/* <View style={styles.container}> */}
            <View style={styles.userContainer}>
                    <Image source={require('./assets/logo.png')} style={{ width: 70, height: 70, borderRadius: 100, marginTop: 0 }} />
                    <View style={{ flexDirection: 'column', paddingLeft: 20, paddingTop:10 }}>
                        <Text style={{ color: "#FFFFFF" , fontWeight: 'bold' }}>Welcome,</Text>
                        <Text style={{ color: "#FFFFFF" , fontWeight: 'bold', paddingTop:5 , fontSize:20 }}>Username</Text>
                    </View>
                </View>
      <FlatList
        data={this.state.data}
        renderItem={({ item }) =>
        < View style={styles.skillContainer}>
            <TouchableOpacity><Image flex={10} source={{ uri: `${item.gambar}` }} style={styles.Image} /></TouchableOpacity>
            <View style={styles.skillDetail}>
                <Text flex={4} style={{ color: "#003366", fontWeight: 'bold', fontSize: 24 }}>{item.productName}</Text>
            </View>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Details'  ,{id: item.id})}>
                <Entypo flex={1} name="chevron-right" size={80} color="#003366" />
            </TouchableOpacity>
          </View>
        }
        // press={
        //     () => {
        //         this.props.navigation.navigate('Detail' ,{
        //             id: item.id,
        //         })
        //     }
        // }
        keyExtractor={({ id }, index) => index}
      />

      </ImageBackground>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'flex-start',
        display: 'flex',
    },
    userContainer: {
        alignSelf: "flex-start",
        flexDirection: 'row',
        paddingLeft: 20,
        paddingTop: 10,
        paddingBottom:20
    },
  viewList: {
    height: 100,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#DDD',
    alignItems: 'center'
  },
  Image: {
    width: 88,
    height: 80,
    borderRadius: 40,
  },
  textItemLogin: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 20,
    fontSize: 16
  },
  textItemUrl: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 12,
    marginTop: 10,
    color: 'blue'
  },
  skillContainer: {
    // display: 'flex',
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    // elevation: 5,
    borderRadius: 10,
    justifyContent: "space-around",
    alignItems: 'center',
    marginBottom: 5,
    margin:5,
    padding:20
},
skillDetail: {
    display: 'flex',
    alignItems: 'flex-start'
}
})