function AscendingTen(val1 = '') {
    var val2 = val1 + 10;
    var hasil = [];
    if (val1 == '') {
        hasil = -1;
    } else {
        for (var i = val1; i < val2; i++) {
            hasil.push(i);
        }
    }
    return hasil;
}

console.log("==================== Soal No. 1 ============================");
// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log("==================== Soal No. 2 ============================");


function DescendingTen(val1 = '') {
    var val2 = val1 + 10;
    var hasil = [];
    if (val1 == '') {
        hasil = -1;
    } else {
        for (var i = val1; i >= val2; i--) {
            hasil.push(i);
        }
    }
    return hasil;
}


console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log("==================== Soal No. 3 ============================");
function ConditionalAscDesc(reference = '', check = '') {
    var hasil = [];
    if (reference == '' || check == '') {
        hasil = -1;
    } else {
        if (check % 2 == 0) {
            var nil10 = reference - 10;
            for (var i = reference; i > nil10; i--) {
                hasil.push(i);
            }
        } else {
            var nil10 = reference + 10;
            for (var i = reference; i < nil10; i++) {
                hasil.push(i);
            }
        }
    }
    return hasil;
}
console.log(ConditionalAscDesc(1, 1));
console.log(ConditionalAscDesc(100, 4));

console.log("==================== Soal No. 4 ============================");


function ularTangga()
{
  var hasil = ''
  for(var i=10; i>0; i--)
  {
    for(var j = 0; j <= 10; j++)
    {
       if(i%2 == 0)
       {
        hasil += i * 10 - j
        hasil += ' '
       }
       else
      {
        hasil += (i*10-9)+j
        hasil += ' '
      }
      
    }
    hasil += '\n'
  }
  return hasil
}
console.log(ularTangga());

// /* 
// Output : 
//   100 99 98 97 96 95 94 93 92 91
//   81 82 83 84 85 86 87 88 89 90
//   80 79 78 77 76 75 74 73 72 71
//   61 62 63 64 65 66 67 68 69 70
//   60 59 58 57 56 55 54 53 52 51
//   41 42 43 44 45 46 47 48 49 50
//   40 39 38 37 36 35 34 33 32 31
//   21 22 23 24 25 26 27 28 29 30
//   20 19 18 17 16 15 14 13 12 11
//   1 2 3 4 5 6 7 8 9 10
// */