console.log("================= SOAL NO.1 ==========================")

class Animal
{
	constructor(brand){
		this._name = brand;
		this._legs = 4
		this._cold_blooded = false
	}

	get name()
	{
		return this._name;
	}

	get legs()
	{
		return this._legs;
	}

	get cold_blooded()
	{
		return this._cold_blooded;
	}

	set name(x) {
    	return this._name = x;
  	}

  	set legs(x) {
    	return this._legs = x;
  	}

  	set cold_blooded(x) {
    	return this._cold_blooded = x;
  	}

}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


/*
Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal. 
Perhatikan bahwa Ape (Kera) merupakan hewan berkaki 2, 
hingga dia tidak menurunkan sifat jumlah kaki 4 dari class Animal. 
class Ape memiliki function yell() yang menampilkan “Auooo” dan 
class Frog memiliki function jump() yang akan menampilkan “hop hop”.
*/

class Frog extends Animal {
	constructor(brand)
	{
		super(brand);
		this._sound = "Hop hop"
	}
	jump()
	{
		console.log(this._sound)
	}
}

class Ape extends Animal {
	constructor(brand)
	{
		super(brand);
		this._legs = 2
		this._sound = "Auooo"
	}
	yell()
	{
		console.log(this._sound)
	}
}


var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("================= SOAL NO.2 ==========================")

class Clock {

    constructor(brand)
    {
    	this.template = brand.template;
    }

    render()
    {
    	var date = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;

	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;

	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;

	    var output = this.template
	      .replace('h', hours)
	      .replace('m', mins)
	      .replace('s', secs);

	    console.log(output);
    }

    start()
    {
    	this.timer = setInterval(this.render.bind(this), 1000)
    }

    stop()
    {
    	clearInterval(this.timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
