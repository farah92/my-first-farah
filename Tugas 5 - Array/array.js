console.log("============== Soal No. 1 (Range) ========================")

function range(startNum, finishNum)
{
	var hasil = [];
	if(startNum == undefined || finishNum == undefined)
	{
		return -1;
	}

	else if(startNum < finishNum)
	{
		for(var a = startNum; a <= finishNum ; a++)
		{
			hasil.push(a)
	    }
	    return hasil
	}
	else 
	{
		for(var a = startNum; a >= finishNum ; a--)
		{
			hasil.push(a)
	    }
	    return hasil
	}
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1


console.log("============== Soal No. 2(Range with Step) ========================")

function rangeWithStep(startNum, finishNum, step)
{
	var hasil = [];
	if(startNum < finishNum)
	{
		for(var a = startNum; a <= finishNum ; a+=step)
		{
			hasil.push(a)
	    }
	    return hasil
	}
	else 
	{
		for(var a = startNum; a >= finishNum ; a-=step)
		{
			hasil.push(a)
	    }
	    return hasil
	}
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log("============== Soal No. 3 (Sum Of Range) ========================")

function sum(startNum, finishNum, step = 1)
{
	var total = 0;

	if(startNum == undefined)
	{
		return 0
	}
	else if(finishNum == undefined || step == undefined)
	{
		return 1
	}
	
	else
	{
		var jumlah = rangeWithStep(startNum,finishNum,step)
		for(i = 0; i <jumlah.length; i++)
		{
	   		total += jumlah[i];
		}
		return total
	}

}


console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


console.log("============== Soal No. 4 (Array Multidimensional) ========================")


function dataHandling(input)
{
	 
    for(var i = 0 ; i <= 3; i++)
    {
		console.log('Nomor ID :' + input[i][0]);
		console.log('Nama Lengkap :' + input[i][1]);
		console.log('TTL :' + input[i][2] +' '+ input[i][3]);
		console.log('Hobi :' + input[i][4]);
		console.log('\n');
	}
}


var input = [
	            ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	            ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	            ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	            ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
           	]	

dataHandling(input);

console.log("============== Soal No. 5 (Balik Kata) ========================")

function balikKata(kalimat)
{
	var akhir = '';
	var panjang = kalimat.length;
	for(var i = panjang-1; i >= 0; i--)
	{
		akhir += kalimat.charAt(i);
	}
	return akhir
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log("============== Soal No. 6 (Metode Array) ========================")
var kalimat = ["0001" , "Roman Alamsyah" , "Bandar Lampung" , "21/05/1989" , "Membaca"]

function dataHandling2(kalimat)
{
	kalimat.splice(1, 2, "Roman Alamsyah Elsyahrawy" , "Provinsi Bandar Lampung") 
	var kalimat1 = kalimat;
	kalimat1.splice(4,1,"Pria","SMA International Metro");
	console.log(kalimat1)
 	var tanggal = kalimat1[3]
 	var ambilBulan = tanggal.split('/');
 	var bulan = ambilBulan[1];
 	switch(bulan)
	{
		case '01' : { bulan = 'Januari'; break; }
		case '02' : { bulan = 'Februari'; break; }
		case '03' : { bulan = 'Maret '; break; }
		case '04' : { bulan = 'April '; break; }
		case '05' : { bulan = 'Mei '; break; }
		case '06' : { bulan = 'Juni '; break; }
		case '07' : { bulan = 'Juli '; break; }
		case '08' : { bulan = 'Agustus '; break; }
		case '09' : { bulan = 'September '; break; }
		case 10 : { bulan ='Oktober '; break; }
		case 11 : { bulan ='November '; break; }
		case 12 : { bulan ='Desember '; break;}
		default : { bulan = 'Tolong Ganti Bulan !!'; }
	}	

	console.log(bulan);
	var hasilBulan = ambilBulan.sort(function (value1, value2) { return value2 - value1 } )
	console.log(hasilBulan);
	console.log(kalimat[3].split('/').join("-"))
	console.log(kalimat1[1].slice(0,15))
}

dataHandling2(kalimat);

