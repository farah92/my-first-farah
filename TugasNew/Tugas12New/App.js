import { StatusBar } from 'expo-status-bar';
import React , {Component} from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity , FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './component/videoitem';
import data from './data.json';

export default class App extends Component{
  // alert(data.kind);
  render(){
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        {/* <Text>Farah</Text> */}
        <Image source={require('./images/logo.png')} style={{width:105,height:22,}} />
        <View style={styles.rightNav}>
          <TouchableOpacity>
            <Icon style={styles.navItem} name="search" size={25}></Icon>
            </TouchableOpacity>
            <TouchableOpacity>
            <Icon style={styles.navItem} name="account-circle" size={25}></Icon>
            </TouchableOpacity>  
        </View>
        
      </View>
      <View style={styles.body}>
        <FlatList 
        data={data.items}
        renderItem={(video)=><VideoItem video={video.item}/>}
        keyExtractor={(item)=>item.id}
        itemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
        />
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="home" size={25}></Icon>
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="whatshot" size={25}></Icon>
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="subscriptions" size={25}></Icon>
          <Text style={styles.tabTitle}>Subscription</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="folder" size={25}></Icon>
          <Text style={styles.tabTitle}>Library</Text>
        </TouchableOpacity>

      </View>
      
      {/* <StatusBar style="auto" /> */}
    </View>
    ); }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:10
  },
  navBar:{
    height:55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    paddingTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightNav: {
    flexDirection: 'row',
  },
  navItem: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'


  },
  tabItem:{
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle:{
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4
  }
  
});
